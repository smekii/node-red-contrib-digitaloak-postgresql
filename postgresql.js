module.exports = function(RED) {
    "use strict";
    const { PostgreClient } = require("./PostgreClient.js");
    const { PostgrePool } = require("./PostgrePool.js");
    var EVENT = require("./events.js");
    var STATUS = EVENT;
    const { pg_err_obj_to_string } = require("./utils.js");

    function PostgreSQLConnectionManagerNode(n) {
        const node = this;
        RED.nodes.createNode(node, n);
        
        // Read configuration from editor
        node.ssl_opts = {};
        if (n.tls) {
            node.tlsNode = RED.nodes.getNode(n.tls);
            node.tlsNode.addTLSOptions(node.ssl_opts);
        }
        node.name = n.name;
        node.host = n.host;
        node.port = n.port;
        node.database = n.database;
        node.pool_max_clients = n.pool_max_clients;
        node.pool_client_max_idle = n.pool_client_max_idle;
        node.client_connection_timeout_millis = n.client_connection_timeout_millis;
        node.client_query_timeout = n.client_query_timeout;
        node.client_statement_timeout = n.client_statement_timeout;
        node.is_client_enabled = n.is_client_enabled === "1" ? true : false;
        //

        node.current_status = STATUS.CLIENT_DISCONNECTED;
        node.pool_current_status = STATUS.CLIENT_DISCONNECTED;


        if (node.credentials) {
            node.user = node.credentials.user;
            node.password = node.credentials.password;
        }

        // General config
        const config = {
            host: node.host,
            port: node.port,
            user: node.user,
            password: node.password,
            database: node.database
        };
        
        if (node.client_connection_timeout_millis) config.connectionTimeoutMillis = node.client_connection_timeout_millis;
        if (node.client_query_timeout) config.query_timeout = node.client_query_timeout;
        if (node.client_statement_timeout) config.statement_timeout = node.client_statement_timeout;
        
        if (n.use_tls && node.ssl_opts.rejectUnauthorized) {
            if (!node.ssl_opts.cert) node.error(`connectionManager: '${node.id}' (${node.name}), message: Unable to connect to the server, provide certificate.`);
            if (!node.ssl_opts.ca) node.error(`connectionManager: '${node.id}' (${node.name}), message: Unable to connect to the server, provide CA certificate.`);
            if (!node.ssl_opts.key) node.error(`connectionManager: '${node.id}' (${node.name}), message: Unable to connect to the server, provide certificate key.`);
            config.ssl = {
                servername: node.ssl_opts.servername,
                rejectUnauthorized: node.ssl_opts.rejectUnauthorized,
                ca: node.ssl_opts.ca,
                key: node.ssl_opts.key,
                cert: node.ssl_opts.cert
            }
        } else if (n.use_tls && !node.ssl_opts.rejectUnauthorized) {
            if (!node.ssl_opts.cert) node.error(`connectionManager: '${node.id}' (${node.name}), message: Unable to connect to the server, provide certificate.`);
            if (!node.ssl_opts.key) node.error(`connectionManager: '${node.id}' (${node.name}), message: Unable to connect to the server, provide certificate key.`);
            config.ssl = {
                servername: node.ssl_opts.servername,
                rejectUnauthorized: node.ssl_opts.rejectUnauthorized,
                key: node.ssl_opts.key,
                cert: node.ssl_opts.cert
            }
        } 

        // Client config
        let client_config = config;

        // Pool config
        let pool_config = config;
        pool_config.idleTimeoutMillis = node.pool_client_max_idle;
        pool_config.max = node.pool_max_clients;

        // Create pool instance
        node.postgre_pool = new PostgrePool(pool_config);

        node.postgre_pool.on(EVENT.POOL_CONNECTED, () => {
            node.pool_current_status = STATUS.POOL_CONNECTED;
            node.log(`connectionManager: '${node.id}' (${node.name}), message: pool connected to the database.`);
        })

        node.postgre_pool.on(EVENT.POOL_DISCONNECTED, (err) => {
            if (node.pool_current_status !== STATUS.POOL_DISCONNECTED) {
                node.pool_current_status = STATUS.POOL_DISCONNECTED;
                !err ? node.log(`connectionManager: '${node.id}' (${node.name}), message: pool disconnected from the database.`) : node.error(`connectionManager: '${node.id}' (${node.name}), message: pool disconnected from the database, ${pg_err_obj_to_string(err)}.`);
            }
        })

        node.postgre_pool.on(EVENT.POOL_CONNECTION_FAILED, (err) => {
            if (node.pool_current_status !== STATUS.POOL_CONNECTION_FAILED) {
                node.pool_current_status = STATUS.POOL_CONNECTION_FAILED;
                node.error(`connectionManager: '${node.id}' (${node.name}), message: pool cannot connect to the database, ${pg_err_obj_to_string(err)}.`);
            }
        });

        node.postgre_pool.connect();

        if (node.is_client_enabled) {
            node.postgre_client = new PostgreClient(client_config);

            node.postgre_client.on(EVENT.CLIENT_CONNECTED, () => {
                node.current_status = STATUS.CLIENT_CONNECTED;
                node.log(`connectionManager: '${node.id}' (${node.name}), message: client connected to the database.`);
            })

            node.postgre_client.on(EVENT.CLIENT_DISCONNECTED, (err) => {
                if (node.current_status !== STATUS.CLIENT_DISCONNECTED) {
                    node.current_status = STATUS.CLIENT_DISCONNECTED;
                    !err ? node.log(`connectionManager: '${node.id}' (${node.name}), message: client disconnected from the database.`) : node.error(`connectionManager: '${node.id}' (${node.name}), message: client disconnected from the database, ${pg_err_obj_to_string(err)}.`);
                }
            })

            node.postgre_client.on(EVENT.CLIENT_CONNECTION_FAILED, (err) => {
                if (node.current_status !== STATUS.CLIENT_CONNECTION_FAILED) {
                    node.current_status = STATUS.CLIENT_CONNECTION_FAILED;
                    node.error(`connectionManager: '${node.id}' (${node.name}), message: client cannot connect to the database, ${pg_err_obj_to_string(err)}.`);
                }
            });

            node.postgre_client.connect(() => node.current_status = STATUS.CLIENT_CONNECTED);
        }

        node.on("close", function(done) {
            const disconnected = new Proxy({ "client": null, "pool": null }, {
                set: (obj, key, val) => {
                    if (typeof val !== 'boolean') throw `Invalid value '${val}' assigned for 'disconnected' constant, only boolean allowed.`;
                    if (!["client", "pool"].includes(key)) throw `Invalid object property '${key}', only 'client' and 'pool' are allowed.`;
                    obj[key] = val;
                    if (node.is_client_enabled && obj["client"] === true && obj["pool"] === true) { if (done) done(); }
                    if (!node.is_client_enabled && obj["pool"] === true) { if (done) done(); }
                    return true;
                },
                get: (obj, prop) => { return obj[prop] }
            });

            // Client disconnect
            if (node.is_client_enabled) {
                node.postgre_client.disconnect(() => {
                    if (node.current_status !== STATUS.CLIENT_DISCONNECTED) node.log(`connectionManager: '${node.id}' (${node.name}), message: client disconnected from the database.`);
                    node.current_status = STATUS.CLIENT_DISCONNECTED;
                    disconnected.client = true;
                });
            }

            // Pool disconnect
            node.postgre_pool.disconnect(() => {
                if (node.pool_current_status !== STATUS.POOL_DISCONNECTED) node.log(`connectionManager: '${node.id}' (${node.name}), message: pool disconnected from the database.`);
                node.pool_current_status = STATUS.POOL_DISCONNECTED;
                disconnected.pool = true;
            });

        });
    }

    RED.nodes.registerType("digitaloak-postgresql-connection-manager", PostgreSQLConnectionManagerNode, {
        credentials: {
            user: { type: "text" },
            password: { type: "password" }
        }
    });



    function PostgreSQLNodeQuery(config) {
        const node = this;
        RED.nodes.createNode(node, config);
        node.server = RED.nodes.getNode(config.server);
        node.current_status = { type: STATUS.POOL_DISCONNECTED, fill: "red", shape: "ring", text: "Disconnected" };
        node.status(node.current_status);
        node.query_curr_exec_counter = 0;

        function is_transaction(query) {
            // query is valid as an object (parameterized queries) and as a string (simple queries)
            return typeof query === "string" && query.split(';')[0].match(/[\s]*BEGIN[\s]*/i) || (typeof query.text === "string" && query.text.split(';')[0].match(/[\s]*BEGIN[\s]*/i)) ? true : false;
        }

        function is_query_property_valid(query) {
            return typeof query === "undefined" || query === null 
            || (typeof query === "string" && query.length === 0) 
            || (typeof query === "object" && (typeof query.text !== "string" || (typeof query.text === "string" && (typeof query.types !== "object" && !Array.isArray(query.values)))))
            ? false : true;
        }

        function handle_status_connected() {
            if (node.current_status.type !== STATUS.POOL_CONNECTED) {
                node.current_status = { type: STATUS.POOL_CONNECTED, fill: "green", shape: "ring", text: "Connected" };
                node.status(node.current_status);
            }
        }

        function handle_status_pool_stats(pool_stats) {
            node.status_ = { type: STATUS.POOL_STATS, fill: "green", shape: "ring", text: `Connected ${pool_stats.total_count - pool_stats.idle_count}/${pool_stats.idle_count}/${pool_stats.total_count}\(${pool_stats.waiting_count}\)` };
            if (node.current_status.type !== STATUS.POOL_QUERY_EXECUTING && node.current_status.text !== node.status_.text) {
                node.current_status = (node.status_);
                node.status(node.current_status);
            }
        }

        function handle_status_connecting() {
            node.current_status = { type: STATUS.POOL_CONNECTING, fill: "yellow", shape: "ring", text: "Connecting" };
            node.status(node.current_status);
        }

        function handle_status_disconnected() {
            node.current_status = { type: STATUS.POOL_DISCONNECTED, fill: "red", shape: "ring", text: "Disconnected" };
            node.status(node.current_status);
        }

        function handle_status_disconnecting() {
            node.current_status = { type: STATUS.POOL_DISCONNECTING, fill: "grey", shape: "ring", text: "Disconnecting" };
            node.status(node.current_status);
        }

        function handle_status_connection_failed() {
            node.current_status = { type: STATUS.POOL_CONNECTION_FAILED, fill: "red", shape: "ring", text: "Connection failed" };
            node.status(node.current_status);
        }

        function handle_status_query_executing() {
            node.query_curr_exec_counter++;
            node.current_status = { type: STATUS.POOL_QUERY_EXECUTING, fill: "green", shape: "ring", text: `Executing ${node.query_curr_exec_counter}` };
            node.status(node.current_status);
        }

        function handle_status_query_executed() {
            node.query_curr_exec_counter--;
            if (node.query_curr_exec_counter === 0) {
                node.current_status = node.status_ || {};
                node.status(node.current_status);
            }
            else {
                node.current_status = { type: STATUS.POOL_QUERY_EXECUTING, fill: "green", shape: "ring", text: `Executing ${node.query_curr_exec_counter}` };
                node.status(node.current_status);
            }
        }

        function handle_status_query_error(callback) {
            node.current_status = { type: STATUS.POOL_QUERY_ERROR, fill: "red", shape: "ring", text: `Executing error` };
            node.status(node.current_status);
            callback();
        }

        function subscribe_node_to_pool_events() {
            node.server.postgre_pool.on(EVENT.POOL_CONNECTED, handle_status_connected);
            node.server.postgre_pool.on(EVENT.POOL_STATS, handle_status_pool_stats);
            node.server.postgre_pool.on(EVENT.POOL_CONNECTING, handle_status_connecting);
            node.server.postgre_pool.on(EVENT.POOL_DISCONNECTED, handle_status_disconnected);
            node.server.postgre_pool.on(EVENT.POOL_DISCONNECTING, handle_status_disconnecting);
            node.server.postgre_pool.on(EVENT.POOL_CONNECTION_FAILED, handle_status_connection_failed);
        }

        function unsubscribe_node_from_pool_events() {
            node.server.postgre_pool.removeListener(EVENT.POOL_CONNECTED, handle_status_connected);
            node.server.postgre_pool.removeListener(EVENT.POOL_STATS, handle_status_pool_stats);
            node.server.postgre_pool.removeListener(EVENT.POOL_CONNECTING, handle_status_connecting);
            node.server.postgre_pool.removeListener(EVENT.POOL_DISCONNECTED, handle_status_disconnected);
            node.server.postgre_pool.removeListener(EVENT.POOL_DISCONNECTING, handle_status_disconnecting);
            node.server.postgre_pool.removeListener(EVENT.POOL_CONNECTION_FAILED, handle_status_connection_failed);
        }

        subscribe_node_to_pool_events();

        node.on('input', async function(msg, send, done) {
            let is_executing_query = false;
            try {
                if (!is_query_property_valid(msg.query)) throw "msg.query property is invalid, should contain valid query.";
                if ([STATUS.POOL_CONNECTED].includes(node.server.pool_current_status)) {
                    handle_status_query_executing();
                    is_executing_query = true;
                    let res;
                    if (is_transaction(msg.query)) {
                        res = await node.server.postgre_pool.tx(msg.query);
                    } else {
                        res = await node.server.postgre_pool.query(msg.query);
                    }
                    send = send || function() { node.send.apply(node, arguments) };
                    msg.payload = res;
                    send(msg);
                    if (done) done();
                }
                else{
                    throw "Database is disconnected."
                }
            }
            catch (err) {
                handle_status_query_error(() => {
                    let err_ = `queryNode: '${node.id}' (${node.name}), ${pg_err_obj_to_string(err)}.`;
                    done ? done(err_) : node.error(err_, msg);
                });
            }
            finally {
                if (is_executing_query) handle_status_query_executed();
            }
        });

        node.on("close", (done) => {
            unsubscribe_node_from_pool_events();
            if (done) done();
        })
    }

    RED.nodes.registerType("digitaloak-postgresql-query", PostgreSQLNodeQuery);



    function PostgreSQLListenNode(config) {
        const node = this;
        RED.nodes.createNode(node, config);
        node.server = RED.nodes.getNode(config.server);
        node.channel = config.channel;
        node.name = node.name || `${node.channel}`;
        node.current_status = STATUS.CLIENT_DISCONNECTED;
        node.status({ type: STATUS.CLIENT_DISCONNECTED, fill: "red", shape: "ring", text: "Disconnected" });

        if (!node.server.is_client_enabled) throw `listenerNode: '${node.id}' (${node.name}), message: cannot subscribe to the channel '${node.channel}', connectionManager '${node.server.id}' (${node.server.name}): client for communication with database is disabled (you can change its status via connection configuration in connectionManager node).`;

        function handle_status_connected() {
            if (node.current_status !== STATUS.CLIENT_CONNECTED && node.current_status !== STATUS.CLIENT_LISTENING) {
                node.server.postgre_client.subscribe(node.channel)
                    .then((is_client_subscribed) => {
                        if (is_client_subscribed) node.log(`connectionManager: '${node.server.id}' (${node.server.name}), message: client subscribed to the channel '${node.channel}'.`);
                        node.log(`listenerNode: '${node.id}' (${node.name}), message: subscribed to the channel '${node.channel}'.`);
                        handle_status_listening();
                    })
                    .catch((err) => node.error(`listenerNode: '${node.id}' (${node.name}), message: cannot subscribe to the channel '${node.channel}', ${pg_err_obj_to_string(err)}.`));
                node.current_status = STATUS.CLIENT_CONNECTED;
                node.status({ type: STATUS.CLIENT_CONNECTED, fill: "green", shape: "ring", text: "Connected" });
            }
        }

        function handle_status_listening() {
            if (node.current_status !== STATUS.CLIENT_LISTENING) {
                node.current_status = STATUS.CLIENT_LISTENING;
                node.status({ type: STATUS.CLIENT_LISTENING, fill: "green", shape: "ring", text: "Listening" });
            }
        }

        function handle_status_connecting() {
            node.current_status = STATUS.CLIENT_CONNECTING;
            node.status({ type: STATUS.CLIENT_CONNECTING, fill: "yellow", shape: "ring", text: "Connecting" });
        }

        function handle_status_disconnected() {
            node.current_status = STATUS.CLIENT_DISCONNECTED;
            node.status({ type: STATUS.CLIENT_DISCONNECTED, fill: "red", shape: "ring", text: "Disconnected" });
        }

        function handle_status_disconnecting() {
            node.current_status = STATUS.CLIENT_DISCONNECTING;
            node.status({ type: STATUS.CLIENT_DISCONNECTING, fill: "grey", shape: "ring", text: "Disconnecting" });
        }

        function handle_status_connection_failed() {
            node.current_status = STATUS.CLIENT_CONNECTION_FAILED;
            node.status({ type: STATUS.CLIENT_CONNECTION_FAILED, fill: "red", shape: "ring", text: "Connection failed" });
        }

        let send_recv_msg = (payload) => node.send({ "payload": payload });

        function subscribe_node_to_client_events() {
            node.server.postgre_client.on(EVENT.CLIENT_CONNECTED, handle_status_connected);
            node.server.postgre_client.on(EVENT.CLIENT_CONNECTING, handle_status_connecting);
            node.server.postgre_client.on(EVENT.CLIENT_DISCONNECTED, handle_status_disconnected);
            node.server.postgre_client.on(EVENT.CLIENT_DISCONNECTING, handle_status_disconnecting);
            node.server.postgre_client.on(EVENT.CLIENT_CONNECTION_FAILED, handle_status_connection_failed);
            node.server.postgre_client.on(node.channel, send_recv_msg);
        }

        function unsubscribe_node_from_client_events() {
            node.server.postgre_client.removeListener(EVENT.CLIENT_CONNECTED, handle_status_connected);
            node.server.postgre_client.removeListener(EVENT.CLIENT_CONNECTING, handle_status_connecting);
            node.server.postgre_client.removeListener(EVENT.CLIENT_DISCONNECTED, handle_status_disconnected);
            node.server.postgre_client.removeListener(EVENT.CLIENT_DISCONNECTING, handle_status_disconnecting);
            node.server.postgre_client.removeListener(EVENT.CLIENT_CONNECTION_FAILED, handle_status_connection_failed);
            node.server.postgre_client.removeListener(node.channel, send_recv_msg);
        }

        subscribe_node_to_client_events();

        if (node.server.current_status === STATUS.CLIENT_CONNECTED) handle_status_connected();

        // On restart there are two invocation, true for "restarted" and "removed" in this order.
        node.on("close", (removed, done) => {
            unsubscribe_node_from_client_events();
            // Unsubscribe on remove, on restart config node is reconnected with all its related (child) nodes
            if (removed)
                node.server.postgre_client.unsubscribe(node.channel)
                .then((is_client_unsubscribed) => {
                    node.log(`listenerNode: '${node.id}' (${node.name}), message: unsubscribed from the channel '${node.channel}'.`);
                    if (is_client_unsubscribed) node.log(`connectionManager: '${node.server.id}' (${node.server.name}), message: client unsubscribed from the channel '${node.channel}'.`);
                })
                .catch((err) => {
                    // Subscription doesn't exists if session is closed so it's desirable situation
                    // This support is added because when you deploying flow where one listener node is removed and one restarted
                    // for one "close" event flag will be set to "removed" and for second not (it's been restarted). On restart client connection
                    // could be closed before deleted node will unsubscribe from the channel.
                    if (!typeof err === 'string' || !err.match(/Client was closed and is not queryable/)) {
                        node.error(`listenerNode: '${node.id}' (${node.name}), message: cannot unsubscribe from the channel '${node.channel}', ${pg_err_obj_to_string(err)}`)
                    }
                })
                .finally(() => { if (done) done() });
            else if (done) done();
        });

    }

    RED.nodes.registerType("digitaloak-postgresql-listen", PostgreSQLListenNode);
    
};
