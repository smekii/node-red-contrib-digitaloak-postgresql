
module.exports = {
    pg_err_obj_to_string: (err) => {
        if (typeof err === 'string') {
            return `message: ${err.replace(":",".")}.` // Keep error text format "prop: value"
        } else {
        	let pg_err_str = "";
        	let err_props = Object.getOwnPropertyNames(err);
        	err_props = err_props.filter((v) => {return v !== "stack"});
        	for(const prop of err_props) {
        	    if (err[prop] !== undefined) pg_err_str += `${prop}: ${err[prop]}, `;
        	}
        	pg_err_str = pg_err_str.slice(0,-2);
        	return pg_err_str;            
        }
    },
    
    sleep: (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
