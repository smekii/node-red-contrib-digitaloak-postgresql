# node-red-contrib-digitaloak-postgresql
Node-RED PostgreSQL nodes:
- query, for querying database
- listen, for listening messages on specific channel
- connection-manager, for database connections configuration

## Features
- pool realtime usage statistics
- TLS support (using built in tls-config node)
- async messaging
- parameterized queries
- custom type parsers
- transactions
- parameterized transactions

## TODO
- https://gitlab.com/digitaloak/node-red-contrib-digitaloak-postgresql/-/issues

## Install
Install using "Manage palette" or run the following command in your Node-RED user directory (in docker it's /usr/src/node-red):
```
npm install @digitaloak/node-red-contrib-digitaloak-postgresql
```

## Additional info
- The listen node works with its own client, independently of the query node pool

## Usage
Import example data for test drive and use help tab for detailed information about each node. 

![Introduction](https://gitlab.com/digitaloak/multimedia-repo/-/raw/master/source/images/node-red-contrib-digitaloak-postgresql/usage.gif)



Transactions usage:

![Transactions introduction](https://gitlab.com/digitaloak/multimedia-repo/-/raw/master/source/images/node-red-contrib-digitaloak-postgresql/pg-transactions.gif)
